package com.example.jsp.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WelcomeController {

    private String message = "Hello World";


    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {

        Map<String, String> noidea = new HashMap<String, String>();
        noidea.put("Pierwszy", "Pierwszy Obiekt w Mapie");
        noidea.put("Drugi", "Drugi Obiekt w Mapie");

        model.putAll(noidea);
        model.put("message", this.message);
        return "welcome";
    }
}