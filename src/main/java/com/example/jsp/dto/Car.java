package com.example.jsp.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Car {

    private String name = "Bmw";
    private int year = 2003;
    private String size = "Medium";

    public Car(final String name, final int year, final String size) {
        this.name = name;
        this.year = year;
        this.size = size;
    }
}
