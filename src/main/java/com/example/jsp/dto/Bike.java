package com.example.jsp.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Bike {

    private String name;
    private String size;

    public Bike(final String name, final String size) {
        this.name = name;
        this.size = size;
    }
}
